package ru.neoflex.practice.Controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(CalcController.class)
@AutoConfigureMockMvc
public class CalcControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CalcController calcController;

    @Test
    public void sum() throws Exception {
        int a = 15;
        int b = 20;
        int expectedSum = 35;//a+b;
        BDDMockito.given(calcController.Sum(a,b)).willReturn(a+b);
        mockMvc.perform(MockMvcRequestBuilders.get("/plus/{a}/{b}",a, b)).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string(String.valueOf(expectedSum)));
    }

    @Test
    public void min() throws Exception {
        int a = 20;
        int b = 15;
        int expectedMin = 5;//a-b;
        BDDMockito.given(calcController.Min(a,b)).willReturn(a-b);
        mockMvc.perform(MockMvcRequestBuilders.get("/minus/{a}/{b}", a, b)).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string(String.valueOf(expectedMin)));
    }
}